/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.fonixord.difftracker

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.time.LocalDateTime


class ParticipantAdapter(private val myList: ArrayList<Participant>) :
    RecyclerView.Adapter<ParticipantAdapter.ParticipantViewHolder>() {

    lateinit var listener: MyInterface
    fun participantsFinished (): Int{
        var count = 0
        var lap = 1
        for (p in myList) {
            if (p.lap2Start != null)
                lap = 2
        }
        for (p in myList) {
            if (lap == 1) {
                if (p.lap1End != null)
                    ++count
            }
            else {
                if (p.lap2End != null)
                    ++count
            }
        }
        return count
    }

    fun myAdapter(listener: MyInterface){
        this.listener = listener
    }

    interface MyInterface {
        fun updateSaveButtonState(state: Boolean)
        fun startButtonEnabled(state: Boolean)
    }

    // Describes an item view and its place within the RecyclerView
    class ParticipantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val participantNameView: TextView = itemView.findViewById(R.id.participantName)
        private val participantLap1View: TextView = itemView.findViewById(R.id.participantLap1)
        private val participantLap2View: TextView = itemView.findViewById(R.id.participantLap2)
        private val participantDiffView: TextView = itemView.findViewById(R.id.participantDiff)
        val participantFinish: Button = itemView.findViewById(R.id.participantFinish)

        fun bind(currentParticipant: Participant) {
            participantNameView.text = "${adapterPosition+1}. ${currentParticipant.name}"
            participantLap1View.text = currentParticipant.getLap1Time(false)
            participantLap2View.text = currentParticipant.getLap2Time(false)
            participantDiffView.text = currentParticipant.getLapDiff(false)
        }
    }

    // Returns a new ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParticipantViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.participant_item, parent, false)

        return ParticipantViewHolder(view)
    }

    // Returns size of data list
    override fun getItemCount(): Int {
        return myList.size
    }

    // Displays data at a certain position
    override fun onBindViewHolder(holder: ParticipantViewHolder, position: Int) {
        holder.bind(myList[position])

        //val animation: Animation = AnimationUtils.loadAnimation(holder.itemView.context, android.R.anim.slide_in_left)
        //holder.itemView.startAnimation(animation)

        holder.participantFinish.isEnabled = myList[position].btnEnabled
        holder.participantFinish.text = myList[position].btnText
        val currentItem = myList[position]

        holder.participantFinish.setOnClickListener{
            if(currentItem.lap1Start == null){
                myList.removeAt(position)
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, itemCount)
            } else {
                if (currentItem.lap1End == null){
                    currentItem.lap1End = LocalDateTime.now()
                    currentItem.btnText = "Lap 2"
                    if (currentItem.lap2Start == null || LocalDateTime.now().isBefore(currentItem.lap2Start)){   //Only disable if lap 2 hasn't started
                        currentItem.btnEnabled = false
                    }
                    enableStartButton()
                } else {
                    currentItem.lap2End = LocalDateTime.now()
                    currentItem.btnEnabled = false
                    if (participantsFinished() == myList.size) { // If last participant on last lap
                        //Enable save button
                        listener.updateSaveButtonState(true)
                    }
                    enableStartButton()
                }
                notifyItemChanged(position)
                // If last participant to finish, enable lap2 start button
            }
        }
    }
    private fun enableStartButton() {
        if (participantsFinished() == myList.size){
            //Enable save button
            listener.startButtonEnabled(true)
        }
    }
}