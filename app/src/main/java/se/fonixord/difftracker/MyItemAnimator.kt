package se.fonixord.difftracker

import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView

class MyItemAnimator : DefaultItemAnimator()
{
    override fun animateAdd(holder: RecyclerView.ViewHolder?): Boolean {
        val animation: Animation = AnimationUtils.loadAnimation(holder?.itemView?.context, android.R.anim.slide_in_left)
        holder?.itemView?.startAnimation(animation)
        return super.animateAdd(holder)
    }

    override fun animateRemove(holder: RecyclerView.ViewHolder?): Boolean {
        val animation: Animation = AnimationUtils.loadAnimation(holder?.itemView?.context, android.R.anim.slide_out_right)
        holder?.itemView?.startAnimation(animation)
        return super.animateRemove(holder)
    }
}