package se.fonixord.difftracker

import java.time.Duration
import java.time.LocalDateTime
import kotlin.math.roundToInt
import org.apache.commons.lang3.time.DurationFormatUtils

class Participant(_name: String){
    val name : String = _name
    //Lap 1 variables
    var lap1Start: LocalDateTime? = null
    var lap1End: LocalDateTime? = null
    var lap1Time: Duration = Duration.ZERO
    //Lap 2 Variables
    var lap2Start: LocalDateTime? = null
    var lap2End: LocalDateTime? = null
    var lap2Time: Duration = Duration.ZERO
    //Difference
    var lapDiff: Duration = Duration.ZERO
    //Properties
    var btnEnabled: Boolean = true
    var btnText: String = "DEL"


    //Lap 1
    fun setLap1Time () {
        lap1Time = if(lap1Start != null && lap1End != null) {
            Duration.between(lap1Start, lap1End)
        } else
            Duration.ZERO
    }

    fun getLap1Time (forSaving: Boolean) : String{
        if(lap1Time == Duration.ZERO)
                    setLap1Time()
        if(lap1Time != Duration.ZERO) {
            return if(forSaving)
                DurationFormatUtils.formatDuration(lap1Time.toMillis(), "mm:ss.SSS")
            else
                DurationFormatUtils.formatDuration(lap1Time.toMillis(), "mm:ss")
        } else{     //Run if start or end times are missing
            return if(forSaving)
                    "00:00.000"
                else
                    "00:00"
            }
    }


    //LAP 2
    fun setLap2Time () {
        lap2Time = if(lap2Start != null && lap2End != null) {
            Duration.between(lap2Start, lap2End)
        } else
            Duration.ZERO
    }

    fun getLap2Time (forSaving: Boolean) : String{
        if(lap2Time == Duration.ZERO)
            setLap2Time()
        return if(lap2Time != Duration.ZERO) {
            if(forSaving)
                DurationFormatUtils.formatDuration(lap2Time.toMillis(), "mm:ss.SSS")
            else
                DurationFormatUtils.formatDuration(lap2Time.toMillis(), "mm:ss")
        } else{     //Run if start or end times are missing
            return if(forSaving)
                "00:00.000"
            else
                "00:00"
        }
    }

    fun setLapDiff (){
        lapDiff = if (lap1Time > lap2Time) {
            lap1Time - lap2Time
        } else {
            lap2Time - lap1Time
        }
    }


    //DIFFERENCE
    fun getLapDiff (forSaving: Boolean) : String{
        lap2Time = if(lap2Start != null && lap2End != null) {
            if(lapDiff == Duration.ZERO)
                setLapDiff()
            return if(forSaving)
                DurationFormatUtils.formatDuration(lapDiff.toMillis(), "mm:ss.SSS")
            else
                DurationFormatUtils.formatDuration(lapDiff.toMillis(), "mm:ss")
        } else {
            return if(forSaving)
                "00:00.000"
            else
                "00:00"
        }
    }
}