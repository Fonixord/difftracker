package se.fonixord.difftracker

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import se.fonixord.difftracker.databinding.FragmentResultsBinding
import se.fonixord.difftracker.databinding.FragmentTimerBinding
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.collections.ArrayList

class TimerFragment : Fragment(R.layout.fragment_timer) {
    private var iterations = 0
    private var myTimer: CountDownTimer? = null
    private var lap = 0
    lateinit var participantsList: ArrayList<Participant>  //Could it be null?
    lateinit var recyclerAdapter: ParticipantAdapter

    private var _binding: FragmentTimerBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTimerBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("btnStartState", binding.btnStart.isEnabled)
        outState.putString("btnStartText", binding.btnStart.text.toString())
        outState.putString("tvNextStarterText", binding.tvNextStarter.text.toString())
        outState.putBoolean("tvNextInfoVisibility", binding.tvNextInfo.isVisible)
        //outState.putFloat("tvwTimeTextSize", tvwTime.textSize/ resources.displayMetrics.scaledDensity)  //Returns pixels by default, this converts to sp
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        var theViewModel = ViewModelProvider(requireActivity())[DataViewModel::class.java]
        participantsList = theViewModel.liveParticipantList.value!! //TODO: Non-null-asserted
        recyclerAdapter = ParticipantAdapter(participantsList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(savedInstanceState != null){
            binding.btnStart.isEnabled = savedInstanceState.getBoolean("btnStartState")
            binding.btnStart.text = savedInstanceState.getString("btnStartText")
            binding.tvNextStarter.text = savedInstanceState.getString("tvNextStarterText")
            binding.tvNextInfo.isVisible = savedInstanceState.getBoolean("tvNextInfoVisibility")
            //tvwTime.textSize = savedInstanceState.getFloat("tvwTimeTextSize")
        }

        retainInstance = true   //Make sure it isn't destroyed when screen is rotated to avoid crashes

        binding.btnStart.setOnClickListener {
            if (lap != 2) {
                if (participantsList.size == 0)
                    Toast.makeText(requireContext(), "No Participants", Toast.LENGTH_SHORT).show()  //TODO(Have btnStart be disabled instead)
                else {
                    timerStartPrep()
                }
            } else {
                resetCompetition()
            }
        }
    }

    private fun startTimer(timerLength: Long) {
        val mediaPlayerFull: MediaPlayer = MediaPlayer.create(activity, R.raw.newsound)

        myTimer = object : CountDownTimer(timerLength, 10) {
            override fun onTick(millisUntilFinished: Long) {
                var durationRemaining: Duration
                if(LocalDateTime.now().isAfter(
                    if (lap == 1)
                        participantsList[iterations].lap1Start
                    else
                        participantsList[iterations].lap2Start)
                ){
                    //START PARTICIPANT
                    Log.i("TAG", "iterations: $iterations")
                    participantsList[iterations].btnEnabled = true
                    callback.itemChanged(iterations)
                    if (iterations < participantsList.lastIndex){
                        //More participants
                        Log.i("[NEXT]", (iterations + 1).toString() + " " + participantsList[iterations + 1].name)    //Log next starter
                        binding.tvNextStarter.text = participantsList[iterations + 1].name  //Update display to next participant
                        if(requireContext().resources.configuration.smallestScreenWidthDp >= 600)
                            callback.updateNextUpText(participantsList[iterations + 1].name)    //Update tablet text to match
                    } else {    
                        //FINISH (Last participant has started)
                        cancelTimer()
                        //iterations = 0
                        binding.tvNextStarter.text = null
                        binding.tvNextInfo.isVisible = false
                        binding.tvwTime.text = getString(R.string.participantsStarted)
                        if (participantsList[0].lap2Start != null){ //Lap 2 has started
                            binding.btnStart.text = getString(R.string.resetTimes)  //Change start button to reset
                        }
                        if(requireContext().resources.configuration.smallestScreenWidthDp >= 600){
                            callback.updateNextUpText("")
                            if(participantsList[0].lap2Start != null){
                                callback.updateCountdownText("00")
                            }
                        }

                    }
                    iterations++    //Increment to next participant
                    if (iterations > participantsList.lastIndex){
                        iterations = 0
                    }
                }else { //Avoids updating the text after all participants have finished. Otherwise creates visual bug.
                    //Update display for time remaining
                    durationRemaining = Duration.between(LocalDateTime.now(), //Calculate time left until next start
                            if(lap == 1)
                                participantsList[iterations].lap1Start
                            else
                                participantsList[iterations].lap2Start)
                    val secondsRemaining = (durationRemaining.seconds + 1).toString()
                    //val secondsRemaining = (durationRemaining.toMillis().toDouble()/1000).roundToInt().toString()
                    updateCountdown(secondsRemaining)
                    if(requireContext().resources.configuration.smallestScreenWidthDp >= 600)   //If tablet, update tablet text
                        callback.updateCountdownText(secondsRemaining)
                    //Log.i("Info","The value is currently: $secondsRemaining")
                    if (durationRemaining.toMillis().toDouble() in 0.0..2999.0 && !mediaPlayerFull.isPlaying) {
                        mediaPlayerFull.start()
                    }
                }

            }

            override fun onFinish() {
                //Empty
            }
        }
        myTimer?.start()
    }

    fun cancelTimer(){
        myTimer?.cancel()
    }

    private fun timerStartPrep() {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireActivity())
        binding.tvNextInfo.isVisible = true
        for (i in 0..participantsList.lastIndex) {
            if (lap == 0) {
                participantsList[i].btnText = "Lap 1"  //Change text from 'DEL' to 'Lap 1'
                participantsList[i].btnEnabled = false //Disable all buttons to begin with so that they can't be pressed before the participant has started
                callback.itemChanged(i)
            }
        }
        callback.fabButtonEnabled(false)    //Hide FAB, so that extra participants can't be added during the competition.
        var countdownLength: MutableList<Long> = mutableListOf()
        val countdownInterval = sharedPreferences.getString("countdownTime", null)?.toLong()?.times(1000) ?: 60000 //Sets fallback value to 60 seconds (60 000 ms)

        for (part in participantsList) {
            countdownLength.add(countdownInterval)
        }

        if (sharedPreferences.getBoolean("randomIntervals", false)) {
            for (index in 0..countdownLength.size - 1) {
                val randomAddition = (-10..10).random()*1000.toLong()    //Convert to millis
                if (countdownLength[index] + randomAddition > 4000)
                    countdownLength[index] += randomAddition
                else
                    countdownLength[index] = 4000
            }
        }

        updateCountdown("")  //Clear countdown

        participantsList.forEachIndexed { index, part -> //Calculate and save the start time for all the participants. Does not support pausing.
            if(lap == 0){
                part.lap1Start = LocalDateTime.now().plus(countdownLength.subList(0, index + 1).sum(), ChronoUnit.MILLIS)   //index + 1 as it's exclusive
            }
            else if(lap == 1)
                part.lap2Start = LocalDateTime.now().plus(countdownLength.subList(0, index + 1).sum(), ChronoUnit.MILLIS)   //index + 1 as it's exclusive
        }

        //Actual starting of timer
        startTimer(countdownLength.sum() + 5000) //Have the timer running and updating the displayed time for all participants. Should be improved for pausing.
        lap++
        binding.tvNextStarter.text = participantsList[iterations].name
        if(requireContext().resources.configuration.smallestScreenWidthDp >= 600)
            callback.updateNextUpText(participantsList[iterations].name)    //Update tablet text to match
        binding.btnStart.isEnabled = false
        binding.btnStart.text = getString(R.string.startLap2)
    }

    private fun updateCountdown(message: String){
        binding.tvwTime.text = message
    }

    private fun resetCompetition() {
        participantsList.clear()
        for(i in 0..participantsList.lastIndex)
            callback.itemChanged(i)
        binding.btnStart.isEnabled = false
        binding.btnStart.text = getString(R.string.startLap1)
        callback.fabButtonEnabled(true)
        callback.saveButtonEnabled(false)
        updateCountdown("")  //Clear countdown
        if(requireContext().resources.configuration.smallestScreenWidthDp >= 600){ //If tablet
            callback.updateCountdownText("00")  //Update tablet text
        } else {
            binding.tvwTime.text = "00"
        }
        lap = 0
        callback.itemsRemoved()
    }

    //Callback for causing change of state for saveButton
    internal lateinit var callback: TimerFragmentListener

    fun setTimerFragmentListener(callback: TimerFragmentListener) {
        this.callback = callback
    }

    // This interface can be implemented by the Activity, parent Fragment,
    // or a separate test implementation.
    interface TimerFragmentListener {
        fun saveButtonEnabled(state: Boolean)
        fun fabButtonEnabled(state: Boolean)
        fun updateCountdownText(text: String)
        fun updateNextUpText(text: String)
        fun itemChanged(position: Int)
        fun itemsRemoved()
    }

    //Function for changing state of btnStart
    fun updateStartButtonState(state: Boolean){
        binding.btnStart.isEnabled = state
    }
}