package se.fonixord.difftracker

//import android.R
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayoutMediator
import se.fonixord.difftracker.databinding.ActivityMainBinding

//import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), TimerFragment.TimerFragmentListener,
    ResultsFragment.ResultsFragmentListener {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        //setContentView(R.layout.activity_main)

        val theViewModel = ViewModelProvider(this)[DataViewModel::class.java]

        val fragmentList = listOf(
            SettingsFragment(),
            ResultsFragment(),
            TimerFragment()
        )

        val tabTexts = listOf(
            getString(R.string.tabSettings),
            getString(R.string.tabResults),
            getString(R.string.tabTimer)
        )

        val tabIcons = listOf(
            ContextCompat.getDrawable(this, R.drawable.ic_action_settings),
            ContextCompat.getDrawable(this, R.drawable.ic_action_home),
            ContextCompat.getDrawable(this, R.drawable.ic_action_timer)
        )

        val adapter = ViewPagerAdapter(fragmentList, supportFragmentManager, lifecycle)
        binding.vpMainContent.adapter = adapter
        binding.vpMainContent.currentItem = 1
        binding.vpMainContent.offscreenPageLimit =
            fragmentList.lastIndex    //Keep the other (two) pages loaded

        TabLayoutMediator(binding.tlMainContent, binding.vpMainContent) { tab, position ->
            binding.vpMainContent.setCurrentItem(tab.position, true)
            tab.text = tabTexts[position]
            tab.icon = tabIcons[position]
        }.attach()

    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Did You Mean To Quit?")
            .setMessage("Results that have not been saved will be lost.")
            .setPositiveButton(
                "Yes",
                DialogInterface.OnClickListener { dialog, which -> finish() })
            .setNegativeButton("No", null)
            .show()
    }

    override fun onAttachFragment(fragment: Fragment) {
        if (fragment is TimerFragment) {
            fragment.setTimerFragmentListener(this)
        }
        if (fragment is ResultsFragment) {
            fragment.setResultsFragmentListener(this)
        }
    }

    override fun startButtonEnabled(state: Boolean) {
        //Actions on callback
        val timerFragment = supportFragmentManager.fragments[2] as TimerFragment?
        if (timerFragment != null)
            timerFragment.updateStartButtonState(state)
    }

    override fun saveButtonEnabled(state: Boolean) {
        val resultsFragment = supportFragmentManager.fragments[1] as ResultsFragment?
        if (resultsFragment != null)
            resultsFragment.updateSaveButtonState(state)
    }

    override fun fabButtonEnabled(state: Boolean) {
        val resultsFragment = supportFragmentManager.fragments[1] as ResultsFragment?
        if (resultsFragment != null)
            resultsFragment.updateFABState(state)
    }

    override fun updateCountdownText(text: String) {
        val resultsFragment = supportFragmentManager.fragments[1] as ResultsFragment?
        if (resultsFragment != null)
            resultsFragment.updateCountdownText(text)
    }

    override fun updateNextUpText(text: String) {
        val resultsFragment = supportFragmentManager.fragments[1] as ResultsFragment?
        if (resultsFragment != null)
            resultsFragment.updateNextUpText(text)
    }

    override fun itemChanged(position: Int) {
        val resultsFragment = supportFragmentManager.fragments[1] as ResultsFragment?
        if (resultsFragment != null)
            resultsFragment.itemChanged(position)
    }

    override fun itemsRemoved() {
        val resultsFragment = supportFragmentManager.fragments[1] as ResultsFragment?
        if (resultsFragment != null)
            resultsFragment.itemsRemoved()
    }
}