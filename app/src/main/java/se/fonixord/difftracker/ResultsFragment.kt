package se.fonixord.difftracker

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.*
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import se.fonixord.difftracker.databinding.FragmentResultsBinding
import java.io.OutputStream
import java.time.LocalDateTime


class ResultsFragment : Fragment(R.layout.fragment_results), ParticipantAdapter.MyInterface{
    private val FILE_SAVE_REQUEST_CODE = 1
    //private val FILE_PERMISSION_REQUEST_CODE = 2
    lateinit var participantsList: ArrayList<Participant>  //Could it be null?
    lateinit var recyclerAdapter: ParticipantAdapter

    private var _binding: FragmentResultsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentResultsBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("saveButtonState", binding.saveButton.isEnabled)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        if(savedInstanceState != null){
            binding.saveButton.isEnabled = savedInstanceState.getBoolean("saveButtonState")
        }

        var theViewModel = ViewModelProvider(requireActivity())[DataViewModel::class.java]
        participantsList = theViewModel.liveParticipantList.value!!    //TODO:Non-null-asserted
        recyclerAdapter = ParticipantAdapter(participantsList)

        binding.recyclerView.itemAnimator = MyItemAnimator()

        if(requireContext().resources.configuration.smallestScreenWidthDp >= 600)
            binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        else
            binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = recyclerAdapter
        recyclerAdapter.myAdapter(this)   //Enable interface for changing save button state

        fun addNewParticipant(name: String) {
            if (participantsList.size == 0)     //Enable start button when there is at least one participant
                callback.startButtonEnabled(true)
            participantsList.add(Participant(name))
            recyclerAdapter.notifyItemInserted(participantsList.lastIndex)
        }

        //OnCLickListener for FAB which adds participants
        binding.fabAddParticipant.setOnClickListener{
            var taskEditText = EditText(activity)   //EditText for name
            taskEditText.setSingleLine()    //Only allow one, though infinitely long, line
            taskEditText.requestFocus()     //Show keyboard automatically #1

            val dialogBuilder: AlertDialog = AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.addNewParticipant))
                    .setView(taskEditText)
                    .setPositiveButton(getString(R.string.add)) { dialog, which ->
                        val taskInput: String = taskEditText.text.toString()
                        if (taskInput == ""){    //Stop empty names
                            Toast.makeText(requireContext(), "Empty Name", Toast.LENGTH_SHORT).show()
                            return@setPositiveButton
                        } else if (taskInput.length > 40) {  //Stop very long names (based on file saving capabilities)
                            Toast.makeText(requireContext(), "Name Too Long", Toast.LENGTH_LONG).show()
                            return@setPositiveButton
                        } else{
                            addNewParticipant(taskInput)
                        }
                    }
                    .setNegativeButton(getString(R.string.cancel), null)
                    .create()
            dialogBuilder.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE) //Show keyboard automatically #2
            val dialog: AlertDialog = dialogBuilder
            dialog.show()

            taskEditText.setOnKeyListener { v, keyCode, event ->     //Enable ENTER/CONFIRM button on keyboard to accept input
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (taskEditText.text.toString() == ""){    //Stop empty names
                        Toast.makeText(requireContext(), "Empty Name", Toast.LENGTH_SHORT).show()
                        false
                    } else if (taskEditText.text.toString().length > 40) {  //Stop very long names (based on file saving capabilities)
                        Toast.makeText(requireContext(), "Name Too Long", Toast.LENGTH_LONG).show()
                        false
                    } else{
                        dialog.dismiss()
                        addNewParticipant(taskEditText.text.toString())
                        true
                    }
                } else false
            }
        }

        //Hide fab when scrolling down, and show it when scrolling up
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) binding.fabAddParticipant.hide() else if (dy < 0) binding.fabAddParticipant.show()
            }
        })

        //Save results to file
        binding.saveButton.setOnClickListener {
            //val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            //startActivityForResult(intent, FILE_PERMISSION_REQUEST_CODE)  //Gives permission to the selected directory
            pickFileLocation()
        }
    }

    //Allow user to pick save location for results file
    private fun pickFileLocation() {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireActivity())
        val timeMoment = LocalDateTime.now()    //Get current time, for backup naming
        val fileName = sharedPreferences.getString(
            "competitionName",
            (timeMoment.year.toString() + timeMoment.monthValue + timeMoment.dayOfMonth + "_" + timeMoment.hour + timeMoment.minute)
        ) + ".csv"  //If the user has set a name for the competition, use that. Otherwise, use the current year-month-day_hour-minute
        val state = Environment.getExternalStorageState()   //Check availability of external storage
        if (Environment.MEDIA_MOUNTED != state)     //Return if unable to write
            return
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)  //Intent to create a new file
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.putExtra(Intent.EXTRA_TITLE, fileName)
        intent.type = "*/*"   //Without this it crashes, specifies that it can be/to show all file types TODO(Only TXT)
        startActivityForResult(intent, FILE_SAVE_REQUEST_CODE)
    }

    //Called when intents finished
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireActivity())

        if(resultCode == RESULT_OK){
            /*if(requestCode == FILE_PERMISSION_REQUEST_CODE && data != null){
                val uri = data.data?.path
                Log.i("INFO", "URI is $uri")
            } else*/ if (resultCode == RESULT_OK && requestCode == FILE_SAVE_REQUEST_CODE) {  //Check if it's the results file save intent
                val timeMoment = LocalDateTime.now()
                val competitionName = sharedPreferences.getString(
                        "competitionName",      //TODO(Use first start instead)
                        (timeMoment.year.toString() + participantsList[0].lap1Start?.monthValue + participantsList[0].lap1Start?.dayOfMonth + "_" + participantsList[0].lap1Start?.hour + participantsList[0].lap1Start?.minute)
                )   //If the user has set a name for the competition, use that. Otherwise, use the current year-month-day_hour-minute to create a hopefully unique name
                val sortedParticipants = participantsList.sortedBy { it.lapDiff}   //Clone and sort participantsList by difference, with the winner first
                //Build String To Be Stored
                var saveString = "The event $competitionName took place on ${participantsList[0].lap1Start}. These are the results:" +
                        "\nStart Nr.,Name:,Lap 1 Time:,Lap 2 Time:,Difference:"
                for (result in sortedParticipants){
                    var resultString = "\n${participantsList.indexOf(result)+1},${result.name}"    //Store start position and name
                    resultString += ",${result.getLap1Time(true)},${result.getLap2Time(true)},${result.getLapDiff(true)}"   //Add the rest of the data, which should be equally wide
                    saveString += resultString      //Add this built string of all the participants to the existing headers string in saveString
                }
                //Get output stream (the file we're saving to)
                val uri: Uri? = data?.data
                val fos: OutputStream? = uri?.let { context?.contentResolver?.openOutputStream(it) }

                //Write to output stream (file)
                try {
                    if (fos != null) {
                        fos.write(saveString.toByteArray())
                        fos.flush()
                        fos.close()
                    }
                    Toast.makeText(context, "Results Saved", Toast.LENGTH_LONG).show()
                } catch (e: Exception) {
                    Toast.makeText(context, "Unable To Save", Toast.LENGTH_LONG).show()
                    e.printStackTrace()
                }
            }
        }
    }

    //   C A L L   B A C K S
    //Callback for causing change of state for btnStart
    internal lateinit var callback: ResultsFragmentListener

    fun setResultsFragmentListener(callback: ResultsFragmentListener) {
        this.callback = callback
    }

    // This interface can be implemented by the Activity, parent Fragment,
    // or a separate test implementation.
    interface ResultsFragmentListener {
        fun startButtonEnabled(state: Boolean)
    }

    fun itemChanged(position: Int){
        recyclerAdapter.notifyItemChanged(position)
    }

    fun itemsRemoved(){
        recyclerAdapter.notifyDataSetChanged()
    }

    override fun updateSaveButtonState(state: Boolean){
        binding.saveButton.isEnabled = state
    }

    override fun startButtonEnabled(state: Boolean) {
        callback.startButtonEnabled(state)
    }

    fun updateFABState(state: Boolean){
        binding.fabAddParticipant.isEnabled = state
    }
    fun updateCountdownText(text: String){
        try{
            binding.tvResultsCountdown?.text = text
        } catch(e : Exception){
            throw Exception("tvResultsCountdown is null: $e")
        }
    }
    fun updateNextUpText(text: String){
        binding.tvNextUp?.text = text
    }
}