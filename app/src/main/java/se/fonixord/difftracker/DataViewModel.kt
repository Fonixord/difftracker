package se.fonixord.difftracker

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataViewModel : ViewModel() {
    val liveParticipantList: MutableLiveData<ArrayList<Participant>> = MutableLiveData(arrayListOf())
}