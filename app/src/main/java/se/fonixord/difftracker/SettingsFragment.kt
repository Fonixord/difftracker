package se.fonixord.difftracker

import android.os.Bundle
import android.text.InputType
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)

        val countdownTimePref: EditTextPreference? =
            preferenceManager.findPreference("countdownTime")
        val competitionNamePref: EditTextPreference? =
            preferenceManager.findPreference("competitionName")
        val randomCountdownPref: SwitchPreference? =
            preferenceManager.findPreference("randomIntervals")

        countdownTimePref?.setDefaultValue("60")
        countdownTimePref?.setOnBindEditTextListener { EditText ->
            EditText.inputType = InputType.TYPE_CLASS_NUMBER
        }

        countdownTimePref?.onPreferenceChangeListener =
            object : Preference.OnPreferenceChangeListener {
                override fun onPreferenceChange(preference: Preference, newValue: Any?): Boolean {
                    if (newValue.toString().toInt() < 4) {   //If the countdown is set to less than 4 seconds, stop this preference from being saved (old will prevail)
                        return false
                    }
                    return true
                }
            }

        competitionNamePref?.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { preference, newValue ->
                if(newValue == ""){
                    competitionNamePref?.summary = "[Current date]"
                } else {
                    competitionNamePref?.summary = newValue.toString()
                }
                true
            }

        randomCountdownPref?.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { preference, newValue ->
                if (newValue == true) {
                    countdownTimePref?.isEnabled = false
                } else {
                    countdownTimePref?.isEnabled = true
                }
                true
            }
    }
}